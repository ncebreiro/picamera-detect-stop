import numpy as np
import cv2

stop_cascade = cv2.CascadeClassifier('data/stage6.xml')
cap = cv2.VideoCapture(0)

while 1:
    ret, img = cap.read()
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    
    # add this
    # image, reject levels level weights.
    stops = stop_cascade.detectMultiScale(gray, 50, 50)
    
    # add this
    for (x,y,w,h) in stops:
	font = cv2.FONT_HERSHEY_SIMPLEX
        cv2.putText(img,'STOP',(x-w,y-h), font, 0.5, (11,255,255), 2, cv2.LINE_AA)

	#cv2.rectangle(img,(x,y),(x+w,y+h),(255,255,0),2)

    cv2.imshow('img',img)
    k = cv2.waitKey(30) & 0xff
    if k == 27:
        break

cap.release()
cv2.destroyAllWindows()
