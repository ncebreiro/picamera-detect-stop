import cv2
import numpy as np
import os
import sys

def store_raw_images(pic):
	if not os.path.exists('neg'):
        	os.makedirs('neg')
	try:
            img = cv2.imread(pic, cv2.IMREAD_GRAYSCALE)
            # should be larger than samples / pos pic (so we can place our image on it)
            resized_image = cv2.resize(img, (100, 100))
            cv2.imwrite(pic, resized_image)
	except Exception as e:
		print(str(e))

store_raw_images(sys.argv[1])
